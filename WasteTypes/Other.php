<?php
require_once 'Waste.php';
require_once 'Interfaces/IncineratorInterface.php';

class Other extends Waste implements IncineratorInterface {

    public function getAmount()
    {
        $obj = json_decode(file_get_contents('data.json'), true);
        $amount = 0;
        foreach ($obj['quartiers'] as  $key => $val) {
            $amount += $val['autre'];
        }
        return $amount;
    }
    public function getCO2Inc($a) {
        $obj = json_decode(file_get_contents('co2.json'), true);
        $co2 = $obj['autre']['incineration'];
        $amountCo2 = $a * $co2;
        return $amountCo2;
    }
    public function getCO2Rec($a){}
    public function getCO2Com($a){}


    
}

?>
<?php
require_once 'Waste.php';
require_once 'Interfaces/IncineratorInterface.php';
require_once 'Interfaces/RecyclingPETInterface.php';

class PlasticPET extends Waste implements RecyclingPETInterface, IncineratorInterface {

    public function getAmount()
    {
        $obj = json_decode(file_get_contents('data.json'), true);
        $amount = 0;
        foreach ($obj['quartiers'] as  $key => $val) {
            $amount += $val['plastiques']['PET'];
        }
        return $amount;

    }
    public function getCO2Rec($a) {
        $obj = json_decode(file_get_contents('co2.json'), true);
        $co2 = $obj['plastiques']['PET']['recyclage'];
        $amountCo2 = $a * $co2;
        return $amountCo2;
    }
    public function getCO2Inc($a) {
        $obj = json_decode(file_get_contents('co2.json'), true);
        $co2 = $obj['plastiques']['PET']['incineration'];
        $amountCo2 = $a * $co2;
        return $amountCo2;
    }
    public function getCO2Com($a){}


}


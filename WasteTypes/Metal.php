<?php
require_once 'Waste.php';
require_once 'Interfaces/IncineratorInterface.php';
require_once 'Interfaces/RecyclingMetalInterface.php';

class Metal extends Waste implements RecyclingMetalInterface, IncineratorInterface {

    public function getAmount()
    {
        $obj = json_decode(file_get_contents('data.json'), true);
        $amount = 0;
        foreach ($obj['quartiers'] as  $key => $val) {
            $amount += $val['metaux'];
        }
        return $amount;
    }
    public function getCO2Rec($a) {
        $obj = json_decode(file_get_contents('co2.json'), true);
        $co2 = $obj['metaux']['recyclage'];
        $amountCo2 = $a * $co2;
        return $amountCo2;
    }
    public function getCO2Inc($a) {
        $obj = json_decode(file_get_contents('co2.json'), true);
        $co2 = $obj['metaux']['incineration'];
        $amountCo2 = $a * $co2;
        return $amountCo2;
    }
    public function getCO2Com($a){}

}

?>
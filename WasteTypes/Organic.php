<?php
require_once 'Waste.php';
require_once 'Interfaces/ComposterInterface.php';


class Organic extends Waste implements ComposterInterface, IncineratorInterface {


    public function getAmount()
    {
        $obj = json_decode(file_get_contents('data.json'), true);
        $amount = 0;
        foreach ($obj['quartiers'] as  $key => $val) {
            $amount += $val['organique'];
        }
        return $amount;
    }
    public function getCO2Com($a) {
        $obj = json_decode(file_get_contents('co2.json'), true);
        $co2 = $obj['organique']['compostage'];
        $amountCo2 = $a * $co2;
        return $amountCo2;
    }
    public function getCO2Inc($a) {
        $obj = json_decode(file_get_contents('co2.json'), true);
        $co2 = $obj['organique']['incineration'];
        $amountCo2 = $a * $co2;
        return $amountCo2;
    }
    public function getCO2Rec($a){}
}

?>
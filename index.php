<?php
require_once 'Waste.php';
require_once 'Treatment.php';
require_once 'WasteTreatment.php';
require_once 'WasteTypes/PlasticPET.php';
require_once 'WasteTypes/PlasticPC.php';
require_once 'WasteTypes/PlasticPEHD.php';
require_once 'WasteTypes/PlasticPVC.php';
require_once 'WasteTypes/Paper.php';
require_once 'WasteTypes/Glass.php';
require_once 'WasteTypes/Metal.php';
require_once 'WasteTypes/Organic.php';
require_once 'WasteTypes/Other.php';
require_once 'TreatmentType/Incinerator.php';
require_once 'TreatmentType/Composter.php';
require_once 'TreatmentType/RecyclingPC.php';
require_once 'TreatmentType/RecyclingPEHD.php';
require_once 'TreatmentType/RecyclingPET.php';
require_once 'TreatmentType/RecyclingPVC.php';
require_once 'TreatmentType/RecyclingPaper.php';
require_once 'TreatmentType/RecyclingMetal.php';
require_once 'TreatmentType/RecyclingGlass.php';

    
    $wasteTreatment = new WasteTreatment();
    $glass = new Glass('verre');
    $metal = new Metal('metaux');
    $organic = new Organic('organique');
    $paper = new Paper('papier');
    $plasticPC = new PlasticPC('PC');
    $plasticPEHD = new PlasticPEHD('PEHD');
    $plasticPET = new PlasticPET('PET');
    $plasticPVC = new PlasticPVC('PVC');
    $other = new Other('autre');

    
    $t1 = new Composter();
    $t2 = new Incinerator();
    $t3 = new RecyclingGlass();
    $t4 = new RecyclingMetal();
    $t5 = new RecyclingPaper();
    $t6 = new RecyclingPC();
    $t7 = new RecyclingPEHD();
    $t8 = new RecyclingPET();
    $t9 = new RecyclingPVC();





?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="./script.js" defer></script>
    <title>Waste Manager</title>
</head>
<body>
    <div class="container-display-flex">
    <div class="pt-3 text-center">
    <img src="tri-selectif.jpg" style="width: 50%;"  alt="">
    <div>
        <table class="table table-striped  text-center ">
        <thead>
            <tr>
            <th scope="col"></th>
            <th scope="col">Verre</th>
            <th scope="col">Métal</th>
            <th scope="col">Organique</th>
            <th scope="col">Papier</th>
            <th scope="col">PlastiquePC</th>
            <th scope="col">PlastiquePEHD</th>
            <th scope="col">PlastiquePET</th>
            <th scope="col">PlastiquePVC</th>
            <th scope="col">Autre</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row">Quantité des déchets</th>
            <td><?php echo $glass->getAmount(); ?></td>
            <td><?php echo $metal->getAmount(); ?></td>
            <td><?php echo $organic->getAmount(); ?></td>
            <td><?php echo $paper->getAmount(); ?></td>
            <td><?php echo $plasticPC->getAmount(); ?></td>
            <td><?php echo $plasticPEHD->getAmount(); ?></td>
            <td><?php echo $plasticPET->getAmount(); ?></td>
            <td><?php echo $plasticPVC->getAmount(); ?></td>
            <td><?php echo $other->getAmount(); ?></td>
            </tr>
            <tr>
            <th scope="row">CO2 avec l'incinerateur</th>
            <td><?php echo $glass->getCO2Inc($glass->getAmount()); ?></td>
            <td><?php echo $metal->getCO2Inc($metal->getAmount()); ?></td>
            <td><?php echo $organic->getCO2Inc($organic->getAmount()); ?></td>
            <td><?php echo $paper->getCO2Inc($paper->getAmount()); ?></td>
            <td><?php echo $plasticPC->getCO2Inc($plasticPC->getAmount()); ?></td>
            <td><?php echo $plasticPEHD->getCO2Inc($plasticPEHD->getAmount()); ?></td>
            <td><?php echo $plasticPET->getCO2Inc($plasticPET->getAmount()); ?></td>
            <td><?php echo $plasticPVC->getCO2Inc($plasticPVC->getAmount()); ?></td>
            <td><?php echo $other->getCO2Inc($other->getAmount()); ?></td>
            </tr>
            <tr>
            <th scope="row">CO2 avec le recyclage</th>
            <td><?php echo $glass->getCO2Rec($glass->getAmount()); ?></td>
            <td><?php echo $metal->getCO2Rec($metal->getAmount()); ?></td>
            <td><?php echo 0; ?></td>
            <td><?php echo $paper->getCO2Rec($paper->getAmount()); ?></td>
            <td><?php echo $plasticPC->getCO2Rec($plasticPC->getAmount()); ?></td>
            <td><?php echo $plasticPEHD->getCO2Rec($plasticPEHD->getAmount()); ?></td>
            <td><?php echo $plasticPET->getCO2Rec($plasticPET->getAmount()); ?></td>
            <td><?php echo $plasticPVC->getCO2Rec($plasticPVC->getAmount()); ?></td>
            <td><?php echo 0; ?></td>
            </tr>
            <th scope="row">CO2 avec le composteur</th>
            <td></td>
            <td></td>
            <td><?php echo $organic->getCO2Com($organic->getAmount()); ?></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            </tr>
        </tbody>
        </table>
        <div class="card text-center">
        <div class="card-header">
            Waste Treatment
        </div>
        <div class="card-body">
            <form method="POST">
                <label for="waste-select">Choisissez le déchet:</label>
                <select name="wastes" id="waste-select">
                    <option value="">--Please choose an option--</option>
                    <option value="verre">Verre</option>
                    <option value="metaux">Métal</option>
                    <option value="papier">Papier</option>
                    <option value="PC">PC</option>
                    <option value="PEHD">PEHD</option>
                    <option value="PET">PET</option>
                    <option value="PVC">PVC</option>
                    <option value="organique">Organique</option>
                    <option value="autre">Autre</option>

                </select>
                <label for="treatment-select">Choisissez le traitement:</label>
                <select name="treatments" id="treatment-select">
                    <option value="">--Please choose an option--</option>
                    <option value= "recyclage-verre">Recyclage Verre</option>
                    <option value= "recyclage-metal">Recyclage Métal</option>
                    <option value= "recyclage-papier">Recyclage Papier</option>
                    <option value= "recyclage-pc">Recyclage Plactique PC</option>
                    <option value= "recyclage-pehd">Recyclage Plactique PEHD</option>
                    <option value= "recyclage-pet">Recyclage Plactique PET</option>
                    <option value= "recyclage-pvc">Recyclage Plactique PVC</option>
                    <option value="incineration">Incineration</option>
                    <option value="compostage">Compostage</option>
                </select>
                <br>
                <button type="submit" name="calculer" class="btn btn-primary">Calculer</button>
                </form>
        </div>
        <div class="card-footer text-muted">
            <?php if (isset($_POST['calculer'])) {       
                  if ($_POST['wastes'] == 'verre') {$waste = $glass;}
                  if ($_POST['wastes'] == 'metaux') {$waste = $metal;}
                  if ($_POST['wastes'] == 'organique') {$waste = $organic;}
                  if ($_POST['wastes'] == 'papier') {$waste = $paper;}
                  if ($_POST['wastes'] == 'PC') {$waste = $plasticPC;}
                  if ($_POST['wastes'] == 'PEHD') {$waste = $plasticPEHD;}
                  if ($_POST['wastes'] == 'PET') {$waste = $plasticPET;}
                  if ($_POST['wastes'] == 'PVC') {$waste = $plasticPVC;}
                  if ($_POST['wastes'] == 'autre') {$waste = $other;}
                  if ($_POST['treatments'] == 'compostage') {$treatment = $t1;}
                  if ($_POST['treatments'] == 'incineration') {$treatment = $t2;}
                  if ($_POST['treatments'] == 'recyclage-verre') {$treatment = $t3;}
                  if ($_POST['treatments'] == 'recyclage-metal') {$treatment = $t4;}
                  if ($_POST['treatments'] == 'recyclage-papier') {$treatment = $t5;}
                  if ($_POST['treatments'] == 'recyclage-pc') {$treatment = $t6;}
                  if ($_POST['treatments'] == 'recyclage-pehd') {$treatment = $t7;}
                  if ($_POST['treatments'] == 'recyclage-pet') {$treatment = $t8;}
                  if ($_POST['treatments'] == 'recyclage-pvc') {$treatment = $t9;}

                  echo $wasteTreatment->getCO2($treatment, $waste);
                }?>
        </div>
        </div>
        
    </div>
    </div>
    </div>

</body>
</html>
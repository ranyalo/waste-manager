<?php

abstract class Waste 
{
    protected ?string $title;
    protected float $amount;

    public function __construct($title)
    {
        $this->title = $title;
        $this->amount = 0;
    }
    public function getTitle() : string {
        return $this->title;
    }

    abstract public function getAmount();
    abstract public function getCO2Rec($a);
    abstract public function getCO2Inc($a);
    abstract public function getCO2Com($a);
    
}
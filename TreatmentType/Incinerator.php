<?php
require_once 'Treatment.php';
require_once 'Waste.php';
require_once 'Interfaces/IncineratorInterface.php';


class Incinerator extends Treatment {

    public function __construct()
    {
        $this->title = 'incineration';
    }

    public function getCapacity()
    {
        $sumLineCapacity = 0;
        $sumLine = 0;
        $obj = json_decode(file_get_contents('data.json'), true);
        foreach ($obj['services'] as $val) {
            if (in_array('incinerateur', $val)) {
            $sumLine = $val['ligneFour'] * $val['capaciteLigne'];
            $sumLineCapacity += $sumLine;
            }
            
        }
        $this->capacity = $sumLine * $sumLineCapacity;
        return $sumLineCapacity;
    }
    public function setWaste(?Waste $waste): self
    {
        if (!($waste instanceof IncineratorInterface)) {
            throw new Exception("This is not the correct method of treatment", 1);
        }
        return parent::setWaste($waste);
    }

}


?>
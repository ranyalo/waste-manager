<?php
require_once 'Treatment.php';
require_once 'Waste.php';
require_once 'Interfaces/RecyclingMetalInterface.php';


class RecyclingMetal extends Treatment {

    public function __construct()
    {
        $this->title = 'recyclage';
    }
    public function getCapacity()
    {
        $obj = json_decode(file_get_contents('data.json'), true);
        $c = 0;
        foreach ($obj['services'] as $key => $val) {
          if (in_array('recyclageMetaux', $val)) {
          $c += $val['capacite'];  
          }     
        }
        $this->capacity = $c;
        return $this->capacity;      
    }
    public function setWaste(?Waste $waste): self
    {
        if (!($waste instanceof RecyclingMetalInterface)) {
            throw new Exception("This is not the correct method of treatment", 1);
        }
        return parent::setWaste($waste);
    }
    
}

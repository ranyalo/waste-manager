<?php
require_once 'Treatment.php';
require_once 'Waste.php';
require_once 'Interfaces/RecyclingPCInterface.php';


class RecyclingPC extends Treatment {

    public function __construct()
    {
        $this->title = 'recyclage';
    }

    public function getCapacity()
    {
        $obj = json_decode(file_get_contents('data.json'), true);
        $c = 0;
        foreach ($obj['services'] as $key => $val) {
          if (in_array('recyclagePlastique', $val)) {
             if (in_array('PC', $val['plastiques'])) {
                $c += $val['capacite'];
             }
          }     
        }
        $this->capacity = $c;
        return $this->capacity;      
    }
    public function setWaste(?Waste $waste): self
    {
        if (!($waste instanceof RecyclingPCInterface)) {
            throw new Exception("This is not the correct method of treatment", 1);
        }
        return parent::setWaste($waste);
    }

}


?>
<?php
require_once 'Treatment.php';
require_once 'Waste.php';
require_once 'Interfaces/ComposterInterface.php';


class Composter extends Treatment {

    public function __construct()
    {
        $this->title = 'compostage';
    }

    public function getCapacity()
    {
        $capacityC = 0;
        $sum = 0;
        $obj = json_decode(file_get_contents('data.json'), true);
        foreach ($obj['services'] as $val) {
            if(in_array('composteur', $val)) {
            $capacityC = $val['capacite'] * $val['foyers'];
            $sum += $capacityC;
            }
            else {$this->capacity = 0;}
        }
        $this->capacity = $sum;
        return $this->capacity;
    }
    public function setWaste(?Waste $waste): self
    {
        if (!($waste instanceof ComposterInterface)) {
            throw new Exception("This is not the correct method of treatment", 1);
        }
        return parent::setWaste($waste);
    }

}


?>
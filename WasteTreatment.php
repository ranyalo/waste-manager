<?php
require_once 'Waste.php';
require_once 'Treatment.php';
require_once 'WasteTypes/PlasticPET.php';
require_once 'WasteTypes/PlasticPC.php';
require_once 'WasteTypes/PlasticPEHD.php';
require_once 'WasteTypes/PlasticPVC.php';
require_once 'WasteTypes/Paper.php';
require_once 'WasteTypes/Glass.php';
require_once 'WasteTypes/Metal.php';
require_once 'WasteTypes/Organic.php';
require_once 'WasteTypes/Other.php';
require_once 'TreatmentType/Incinerator.php';
require_once 'TreatmentType/Composter.php';
require_once 'TreatmentType/RecyclingPC.php';

class WasteTreatment 
{

    protected int $capacity;
    protected int $amount;

    
    public function getCapacity(Treatment $treatment) {
           
                return $treatment->getCapacity();           
    }

    public function getAmount(Waste $waste) {
   
                return $waste->getAmount();
    }


    public function getCO2(Treatment $treatment, Waste $waste) {
                $treatment->setWaste($waste);
                $capacity = $treatment->getCapacity();
                $amount = $waste->getAmount();
                if (($waste->getTitle() == 'verre') || ($waste->getTitle() == 'metaux')
                    || ($waste->getTitle() == 'papier') || ($waste->getTitle() == 'PC')
                    || ($waste->getTitle() == 'PEHD') || ($waste->getTitle() == 'PET')
                    || ($waste->getTitle() == 'PVC')) {
                    if ($treatment->getTitle() == 'recyclage' && $amount <= $capacity) {
                       echo "Supper vous pouvez recyclé tout la quantité de déchets et le montant de CO2 rejeté est " 
                       .$waste->getCO2Rec($amount); 
                    }
                    elseif ($treatment->getTitle() == 'recyclage' && $amount > $capacity) {
                        $a = $amount - $capacity;
                        echo "Vous pouvez recyclé ". $capacity ." et le montant de CO2 rejeté est "
                        . $waste->getCO2Rec($capacity). " il reste ".$a. " pour incinérer et le montant de CO2 rejeté est "
                        . $waste->getCO2Inc($a); 
                    }
                    elseif ($treatment->getTitle() == 'incineration' && $amount <= $capacity) {
                        echo "Ce n'est pas la meilleure façon de traitement et le montant de CO2 rejeté est "
                        .$waste->getCO2Inc($amount);
                    }
                
                }
                if (($waste->getTitle() == 'organique')) {
                    if ($treatment->getTitle() == 'compostage' && $amount <= $capacity) {
                    echo "Supper vous pouvez traiter tout la quantité de déchets et le montant de CO2 rejeté est " 
                    .$waste->getCO2Com($amount); 
                     }
                     elseif ($treatment->getTitle() == 'compostage' && $amount > $capacity) {
                     $a = $amount - $capacity;
                     echo "Vous pouvez traiter ". $capacity ." et le montant de CO2 rejeté est "
                     . $waste->getCO2Com($capacity). " il reste ".$a. " pour incinérer et le montant de CO2 rejeté est "
                     . $waste->getCO2Inc($a); 
                    }
                    elseif ($treatment->getTitle() == 'incineration' && $amount <= $capacity) {
                        echo "Ce n'est pas la meilleure façon de traitement et le montant de CO2 rejeté est "
                        .$waste->getCO2Inc($amount);
                    }
                }
                if (($waste->getTitle() == 'autre')) {
                    if ($treatment->getTitle() == 'incineration' && $amount <= $capacity) {
                    echo "Le montant de CO2 réjeté est " .$waste->getCO2Inc($amount);
                    } 
                }
                

    }
    

    

}
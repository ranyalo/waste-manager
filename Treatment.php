<?php
require_once 'Waste.php';

abstract class Treatment 
{
    protected ?Waste $waste;
    protected float $capacity;
    protected string $title;

    public function __construct()
    {
        $this->capacity = 0;
    }
    public function getTitle(): string
    {
        return $this->title;
    }

    public function setWaste(?Waste $waste): self
    {
        $this->waste = $waste;

        return $this;
    }

    abstract public function getCapacity();
    
    
}
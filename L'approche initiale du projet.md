L'approche initiale du projet :
==

J’ai divisé en deux parties :
-
#### 1- Partie pour étudier les types de déchets.
#### 2- Partie de l'étude des services de traitement des déchets.

#### Les tâches pour la première partie : 
1- Créer les classes pour les types de déchets.  
2- Définir les propriétés de ces classes.   
3- Définir les méthodes de ces classes.    
4- Afficher les données depuis les fichiers JSON.

#### Les tâches pour la second partie : 
1- Créer les classes ou les interfaces pour les services de traitement des déchets.  
2- Définir les propriétés de ces classes.   
3- Définir les méthodes de ces classes.    
4- Trouver une solution pour mettre en place les relations entre les types des déchets et ses services de traitement.     
5- Trouver une logique appropriée pour calculer la quantité d’émission de CO2 pour chaque type déchet en fonction de la manière dont il est traité.      
5- Faire un interface graphique pour afficher nos résultats.